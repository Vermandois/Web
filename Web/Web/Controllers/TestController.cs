﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Grpc.Core;
using Helloworld;

namespace Web.Controllers
{
    public class TestController : Controller
    {
	    private const string Parameter = "Parameter:";

        // GET: Test
        public ActionResult Test()
        {
	        var tempStr = $"{Parameter} null";
	        ViewBag.LoginState = tempStr;
			return View();
        }
		
	    [HttpPost]
	    public ActionResult Test(FormCollection fc)
		{
			var parameter = fc["parameter"];

			Channel channel = new Channel("127.0.0.1:50051", ChannelCredentials.Insecure);
		    var client = new Greeter.GreeterClient(channel);
		    var reply = client.SayHello(new HelloRequest { Name = parameter });
			
		    ViewBag.LoginState = reply.Message;
		    return View();
	    }
	}
}